# Lab7 - Rollbacks and reliability

## Part 1
All queries go as one transaction on one connection and cursor. 
If transaction failed on any step - it is rollback, otherwise it is commited

## Part 2
Code for creating Inventory table:
```postgresql
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    PRIMARY KEY(username, product)
)
```
Two additional queries added:
```postgresql
SELECT sum(amount)
FROM Inventory 
WHERE username = %(username)s;

INSERT INTO Inventory (username, product, amount) 
VALUES (%(username)s, %(product)s, %(amount)s) 
ON CONFLICT (username, product) 
DO UPDATE SET amount = Inventory.amount + %(amount)s"
```