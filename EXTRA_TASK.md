# Extra task
## Part 1
We can use unique IDs for transactions. 
If transaction is committed to database and 
client tries to make same transaction again, 
server can check transaction ID and cancel 
new transaction if it is repeat.
## Part 2
We can use some methods:
- 2-Phase-Commit - protocol that ensures that transaction is either committed or
rolled back across multiple databases or resources. 
With 2PC, all participants in transaction agree to commit or roll back, 
and coordinator coordinates the process to ensure that all participants either commit or 
roll back the transaction.
- Compensation Transaction - with this approach, if failure occurs during processing of transaction, 
compensating transaction can be executed to undo changes made by the original transaction.